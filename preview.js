/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Views.Base.PreviewView
 * @alias SUGAR.App.view.views.BasePreviewView
 * @extends View.Views.Base.RecordView
 */
({
    extendsFrom: 'RecordView',
    _tplHeaderHtmlName: 'reply-header-html', //taken from reply-action.js

    /**
     * @inheritdoc
     */
    _subjectPrefix: 'LBL_RE', //taken from reply-action.js
    /**
     * The element ID to use to identify the reply content.
     *
     * @inheritdoc
     */
    _contentId: 'replycontent', //taken from reply-action.js
    plugins: ['ToggleMoreLess', 'Editable', 'ErrorDecoration', 'SugarLogic','RelativeTime'],
    fallbackFieldTemplate: 'detail',
    /**
     * Events related to the preview view:
     *  - preview:open                  indicate we must show the preview panel
     *  - preview:render                indicate we must load the preview with a model/collection
     *  - preview:collection:change     indicate we want to update the preview with the new collection
     *  - preview:close                 indicate we must hide the preview panel
     *  - preview:pagination:fire       (on layout) indicate we must switch to previous/next record
     *  - preview:pagination:update     (on layout) indicate the preview header needs to be refreshed
     *  - list:preview:fire             indicate the user clicked on the preview icon
     *  - list:preview:decorate         indicate we need to update the highlighted row in list view
     */

    // "binary semaphore" for the pagination click event, this is needed for async changes to the preview model
    switching: false,

    hiddenPanelExists: false,

    initialize: function(options) {

        // Use preview view if available, otherwise fallback to record view
        var events = {};

        //try for attachment fields but it's not downloading
        //code is copied from email-attachment.js
        events['change ' + this._fileTag] = '_uploadFile';
        events['click [data-action=download]'] = '_downloadFile';

        this.events = _.extend({}, this.events, options.def.events, events);
        this.dataView = 'preview';
        var previewMeta = app.metadata.getView(options.module, 'preview');
        var recordMeta = app.metadata.getView(options.module, 'record');

        if (_.isEmpty(previewMeta) || _.isEmpty(previewMeta.panels)) {
            this.dataView = 'record';
        }

        this._super('initialize', [options]);
        this.meta = _.extend(this.meta, this._previewifyMetadata(_.extend({}, recordMeta, previewMeta)));

        this.action = 'detail';
        this.getEmails();
        this._delegateEvents();
        this.delegateButtonEvents();
        this.show = true;
        this.preview_subject = '';
        this.related_id = '';
        this.related_name = '';
        this.related_type = '';

        /**
         * An array of the {@link #alerts alert} names in this view.
         *
         * @property {Array}
         * @protected
         */
        this._viewAlerts = [];

        /**
         * A collection of alert messages to be used in this view. The alert methods
         * should be invoked by Function.prototype.call(), passing in an instance of
         * a sidecar view. For example:
         *
         *     // ...
         *     this.alerts.showInvalidModel.call(this);
         *     // ...
         *
         * FIXME: SC-3451 will refactor this `alerts` structure.
         * @property {Object}
         */
        this.alerts = {
            showInvalidModel: function() {
                if (!this instanceof app.view.View) {
                    app.logger.error('This method should be invoked by Function.prototype.call(), passing in as argument' +
                    'an instance of this view.');
                    return;
                }
                var name = 'invalid-data';
                this._viewAlerts.push(name);
                app.alert.show(name, {
                    level: 'error',
                    messages: 'ERR_RESOLVE_ERRORS'
                });
            },
            showNoAccessError: function() {
                if (!this instanceof app.view.View) {
                    app.logger.error('This method should be invoked by Function.prototype.call(), passing in as argument' +
                    'an instance of this view.');
                    return;
                }
                // dismiss the default error
                app.alert.dismiss('data:sync:error');
                // display no access error
                app.alert.show('server-error', {
                    level: 'error',
                    messages: 'ERR_HTTP_404_TEXT_LINE1'
                });
                // discard any changes before redirect
                this.handleCancel();
                // redirect to list view
                var route = app.router.buildRoute(this.module);
                app.router.navigate(route, {trigger: true});
            }
        };

        //lame way for getting base url
        this.base_url = window.location.origin + window.location.pathname;

        /**
         * Expand preview panel and shrink list view
         */
        $('#content').find('.headerpane').css({"width":"40%"});
        $('#content').find('.preview-headerbar').css({"width":"60%"});
        $('#content').find('.main-pane.span8').css({"width":"40%"});
        $('#content').find('.uxs-scroll-bar').css({"width":"40%"});
        $('#content').find('.side.sidebar-content.span4').css({"width":"60%"});
        var self = this;
    },


    /**
     * @description For Preview custom List of related Emails in Preview Pane
     * @author Indrajeet Latthe
     */

    getEmails: function () {
        var self = this;

        /**
         * So we have a format for every case subject like [CASE:1419]
         * So I've taken out that number from subject which is field name
         * Fetch all Emails for showing in Accordions which have same case number
         * And the status is not draft (I pnly want sent or replied email)
         * My priority was and is to display all Emails were reply have been given
         * So they are always with the same case number e.g Re:[Case:1419]
         * For making Email access simpler(more like convenient)
         * Because we have to redirect for every individual email to see them
         */
        var name = this.model.get('name');                      //take subject as name
        var posStrt = name.search('\\[');                           //search poistion of '['
        var posEnd = name.search(']');                             //search position of ']'
        name = name.slice(posStrt,posEnd+1);                             //slice the string from '[' to ']'
        var emails = app.data.createBeanCollection('Emails');   //create collection for Emails modules
        emails.fetch();                                         //fetch collection
        var fetchedEmails = emails.fetch({                      //define filter
            view:'record',
            "filter": [
                {'name': {"$contains":name}},             //Case subject contains trimmed case number
                {'status':{"$not_equals":"draft"}}
                ]
        })

        fetchedEmails.xhr.done(function (data){                 //Async fetch using filter
            data.records.sort(function (a,b) {                  //sort collection date wise
                return a.date_entered < b.date_entered;
            })
            _.each(data.records,function (ele) {                //place html description via jquery in an iframe
                $('#'+ele.id+'iframe').html(ele.description_html);
            })

            /**
             * Set the records for iterating in hbs for making accordions
             * and set custom fields for hbs as I removed all default fields from previewpanel
             * as I only need 2 fields subject and related Case (name and id for redirecting)
             */
            self.meta.emails = data.records;
            self.preview_subject = self.model.get('name');
            self.related_id = self.model.get('parent').id;
            self.related_name = self.model.get('parent').name;
            self.related_type = self.model.get('parent').type;

            //if I don't do this things get messy (it doesn't render changes as it asyncs e.g ->
            // if I preview Email for first time nothing will be shown and if I preview another email
            // it will show previous previewd Emails )
            self.render();


            /**
             * If the arrow (for opening accordion is clicked set unread_c field value
             *
             */
            jQuery('.activitystream-posts-comments-container div.accordion-toggle.collapsed i.fa.fa-chevron-down').on('click',function() {
                var email_id = jQuery(this).attr('id');
                var accountsBean = app.data.createBean('Emails', {id: email_id.slice(4, email_id.length)});
                accountsBean.fetch();
                accountsBean.set('unread_c',0);
                accountsBean.set('update_date_modified',false);
                accountsBean.save();
            });

            //Automatically opening the first accordian of email
            $('.preview-pane.active').find('.fa.fa-chevron-down').first().click();

            //If the close button of preview panel(X) is clicked
            //Set the normal widht of list view and preview panel
            $('.btn.btn-invisible.closeSubdetail').on('click',function(){
                $('#content').find('.headerpane').removeAttr("style");
                $('#content').find('.preview-headerbar').removeAttr("style");
                $('#content').find('.uxs-scroll-bar').removeAttr("style");
                $('#content').find('.main-pane.span8').removeAttr("style");
                $('#content').find('.side.sidebar-content.span4').removeAttr("style");
            });


            /**
             * For getting all the reply buttons and set on click on them
             * For every reply button the id is assigned with btn prefix e.g btnasdad-123123-123123-12313
             */
            var btns = $('#content').find('.preview-email-btn');
            _.each(btns,function (btn) {
                $(btn).on('click',function () {

                    //get the id for button and slice it to get only id
                    var email_id = jQuery(this).attr('id');
                    var email = app.data.createBean('Emails', {id: email_id.slice(3, email_id.length)});

                    //fetch the email fields
                    email.fetch({
                        view:'record',
                        success:__bind(function (data,coll) {
                            //console.log('[TEST]',data);
                            var prefill = app.data.createBean('Emails');
                            prefill.copy(data);

                            /**
                             *  start formatting data for reply
                             *  I've used all functions from reply-action.js for formatting the model
                             *  for setting the proper formatted values for reply model
                             */
                            var attachments = self.emailOptionAttachments(data);
                            var subject = self.emailOptionSubject(data);
                            var htmlbody = self.emailOptionDescriptionHtml(data);
                            var body = self.emailOptionDescription(data);
                            //end formatting data for reply

                            //remove the data from to_collection as I'm replying I want from address for to_collection
                            prefill.unset("to_collection");

                            //get the from address from from_collection which consist models and take the first model
                            var to_col = data.get('from_collection').models[0];

                            //change the link to
                            to_col.link.name = 'to';
                            to_col.set('_link','to');


                            //get to_collection and create the from to_col var
                            //create method is present for to_collection (also there is add but add also don't work)
                            var t = data.get('to_collection');
                            t.create(to_col)
                            //console.log('[T]',t);

                            prefill.set('description_html', htmlbody);
                            prefill.set('to_collection',t);
                            prefill.unset('from_collection');
                            prefill.unset('date_sent');
                            prefill.unset('assigned_user_id');
                            prefill.unset('assigned_user_name');
                            prefill.set('description', body);
                            prefill.set('name',subject);
                            prefill.set('unread_c',0);
                            prefill.set('attachments_collection', attachments);
                            // console.log(prefill);
                            app.drawer.open({
                                layout: 'compose-email',
                                context: {
                                    create:true,
                                    model: prefill,
                                    module: 'Emails',
                                }
                            })
                        },this)
                    })
                });
            });
        });
        //if I don't do this things get messy (it doesn't render changes as it asyncs)
        self.render();
    },

    /**
     * @inheritdoc
     *
     * @override Overriding to get preview specific buttons
     */
    toggleButtons: function(enable) {

        if (this.layout.previewEdit) {
            var previewLayout = this.layout.getComponent('preview-header');
            previewLayout.getField('save_button').setDisabled(!enable);
            previewLayout.getField('cancel_button').setDisabled(!enable);
        }
    },

    /**
     * Runs when validation is successful
     * Returns the preview to detail view
     *
     * @override Overriding because we need to trigger 'preview:edit:complete'
     * and not do record view specific actions like: this.inlineEditMode = false;
     */
    handleSave: function() {
        if (this.disposed) {
            return;
        }
        this._saveModel();
        this.layout.trigger('preview:edit:complete');
        this.unsetContextAction();
        this.toggleFields(this.editableFields, false);
        this.toggleLocks(false);
    },

    /**
     * When clicking cancel, return the preview view to detail state
     * and revert the model
     *
     * @override Overriding in order to trigger 'preview:edit:complete'
     */
    cancelClicked: function() {
        this.model.revertAttributes();
        this.toggleFields(this.editableFields, false);
        this.toggleLocks(false);
        this._dismissAllAlerts();
        this.clearValidationErrors(this.editableFields);
        this.unsetContextAction();
        this.layout.trigger('preview:edit:complete');
    },

    /**
     * Add event listeners
     *
     * @private
     */
    _delegateEvents: function() {
        app.events.on('preview:collection:change', this.showPreviousNextBtnGroup, this);
        //FIXME: SC-4915 will delete this listener.
        app.events.on('app:help:shown', function() {
            app.events.trigger('list:preview:decorate', false);
            this.closePreview();
        }, this);

        // TODO: Remove when pagination on activity streams is fixed.
        app.events.on('preview:module:update', this.updatePreviewModule, this);

        if (this.layout) {
            this.layout.on('preview:pagination:fire', this.switchPreview, this);
        }
    },

    /**
     * Setup event listeners for buttons
     *
     * @override Override because we only want to set events if
     * previewEdit is enabled
     */
    delegateButtonEvents: function() {
        if (this.layout && this.layout.previewEdit) {
            this.context.on('button:save_button:click', this.saveClicked, this);
            this.context.on('button:cancel_button:click', this.cancelClicked, this);
            this.layout.on('preview:edit', this.handleEdit, this);
        }
    },

    /**
     * Calls `View.Views.Base.PreviewView#showPreviousNextBtnGroup`.
     *
     * @deprecated since 7.8, will be removed in 7.9.
     * @param {Data.BeanCollection} collection the given collection (unused)
     */
    updateCollection: function(collection) {
        app.logger.warn('View.Views.Base.PreviewView#updateCollection is deprecated since 7.8 and will be' +
            ' removed in 7.9. Since the preview layout now share the view collection, this method is obsolete.');

        this.showPreviousNextBtnGroup();
    },

    // TODO: Remove when pagination on activity streams is fixed.
    updatePreviewModule: function(module) {
        this.previewModule = module;
    },

    filterCollection: function() {
        this.collection.remove(_.filter(this.collection.models, function(model){
            return !app.acl.hasAccessToModel('view', model);
        }, this), { silent: true });
    },

    _renderHtml: function(){
        //if(this.show){
            this.showPreviousNextBtnGroup();
            app.view.View.prototype._renderHtml.call(this);
          //  this.show = true;
//        }
    },

    /**
     * Show previous and next buttons groups on the view.
     *
     * This gets called everytime the collection gets updated. It also depends
     * if we have a current model or layout.
     *
     * TODO we should check if we have the preview open instead of doing a bunch
     * of if statements.
     */
    showPreviousNextBtnGroup: function () {
        if (!this.model || !this.layout) {
            return;
        }

        var collection = this.collection;
        if (!collection || !collection.size()) {
            this.layout.hideNextPrevious = true;
            // Need to rerender the preview header
            this.layout.trigger('preview:pagination:update');
            return;
        }

        var recordIndex = collection.indexOf(collection.get(this.model.id));
        this.layout.previous = collection.models[recordIndex-1] ? collection.models[recordIndex-1] : undefined;
        this.layout.next = collection.models[recordIndex+1] ? collection.models[recordIndex+1] : undefined;
        this.layout.hideNextPrevious = _.isUndefined(this.layout.previous) && _.isUndefined(this.layout.next);

        // Need to rerender the preview header
        this.layout.trigger('preview:pagination:update');
    },

    /**
     * Renders the preview dialog with the data from the current model and collection.
     *
     * @deprecated Deprecated since 7.8.0. Will be removed in 7.10.0.
     * @param model Model for the object to preview
     * @param collection Collection of related objects to the current model
     * @param {Boolean} fetch Optional Indicates if model needs to be synched with server to populate with latest data
     * @param {Number|String} previewId Optional identifier use to determine event origin. If event origin is not the same
     * but the model id is the same, preview should still render the same model.
     * @private
     */
    _renderPreview: function(model, collection, fetch, previewId) {
        app.logger.warn('`Base.PreviewView#_renderPreview` has been deprecated since 7.8.0 and' +
            'will be removed in 7.10.0.');

        var self = this;

        // If there are drawers there could be multiple previews, make sure we are only rendering preview for active drawer
        if(app.drawer && !app.drawer.isActive(this.$el)){
            return;  //This preview isn't on the active layout
        }

        // Close preview if we are already displaying this model
        if (this.model && model && (this.model.get('id') == model.get('id') && previewId == this.previewId)) {
            // Remove the decoration of the highlighted row
            app.events.trigger('list:preview:decorate', false);
            // Close the preview panel
            app.events.trigger('preview:close');
            return;
        }

        if (app.metadata.getModule(model.module).isBwcEnabled) {
            // if module is in BWC mode, just return
            return;
        }

        if (model) {
            // Use preview view if available, otherwise fallback to record view
            var viewName = 'preview',
                previewMeta = app.metadata.getView(model.module, 'preview'),
                recordMeta = app.metadata.getView(model.module, 'record');
            if (_.isEmpty(previewMeta) || _.isEmpty(previewMeta.panels)) {
                viewName = 'record';
            }
            this.meta = this._previewifyMetadata(_.extend({}, recordMeta, previewMeta));
            this.renderPreview(model, collection);
            fetch && model.fetch({
                showAlerts: true,
                view: viewName
            });
        }

        this.previewId = previewId;
    },
    /**
     * Use the given model to render preview.
     * @param {Bean} model Model to render preview
     */
    switchModel: function(model) {
        this.model && this.model.abortFetchRequest();
        this.stopListening(this.model);
        this.model = model;
        this.show = false;
        this.getEmails();
        $('#content').find('.headerpane').css({"width":"40%"});
        $('#content').find('.preview-headerbar').css({"width":"60%"});
        $('#content').find('.main-pane.span8').css({"width":"40%"});
        $('#content').find('.uxs-scroll-bar').css({"width":"40%"});
        $('#content').find('.side.sidebar-content.span4').css({"width":"60%"});
        // Close preview when model destroyed by deleting the record
        this.listenTo(this.model, 'destroy', function() {
            // Remove the decoration of the highlighted row
            app.events.trigger('list:preview:decorate', false);
            // Close the preview panel
            app.events.trigger('preview:close');
        });
    },
    /**
     * Renders the preview dialog with the data from the current model and collection
     *
     * @deprecated Deprecated since 7.8.0. Will be removed in 7.10.0.
     * @param model Model for the object to preview
     * @param collection Collection of related objects to the current model
     */
    renderPreview: function(model, newCollection) {
        app.logger.warn('`Base.PreviewView#renderPreview` has been deprecated since 7.8.0 and' +
            'will be removed in 7.10.0.');

        if(newCollection) {
            this.collection.reset(newCollection.models);
        }

        if (model) {
            this.switchModel(model);
            if (this.layout) {
                this.layout.trigger('previewheader:ACLCheck', model);
            }

            // TODO: Remove when pagination on activity streams is fixed.
            if (this.previewModule && this.previewModule === 'Activities') {
                // We need to set previewEdit to false before render but set
                // hideNextPreview and trigger 'preview:pagination:update' after
                this.layout.previewEdit = false;
                this.render();
                this.layout.hideNextPrevious = true;
                this.layout.trigger('preview:pagination:update');
            } else {
                // If we aren't on activitystream, then just render
                this.render();
            }
            // Open the preview panel
            app.events.trigger('preview:open', this);
            // Highlight the row
            app.events.trigger('list:preview:decorate', this.model, this);
        }
    },

    /**
     * Normalizes the metadata, and removes favorite/follow fields that gets
     * shown in Preview dialog.
     *
     * @param meta Layout metadata to be trimmed
     * @return Returns trimmed metadata
     * @private
     */
    _previewifyMetadata: function(meta){
        this.hiddenPanelExists = false; // reset
        _.each(meta.panels, function(panel){
            if(panel.header){
                panel.header = false;
                panel.fields = _.filter(panel.fields, function(field){
                    //Don't show favorite or follow in Preview, it's already on list view row
                    return field.type != 'favorite' && field.type != 'follow';
                });
            }
            //Keep track if a hidden panel exists
            if(!this.hiddenPanelExists && panel.hide){
                this.hiddenPanelExists = true;
            }
        }, this);
        return meta;
    },
    /**
     * Switches preview to left/right model in collection.
     * @param {Object} data
     * @param {String} data.direction Direction that we are switching to, either 'left' or 'right'.
     * @param index Optional current index in list
     * @param id Optional
     * @param module Optional
     */
    switchPreview: function(data, index, id, module) {
        var currID = id || this.model.get('id'),
            currIndex = index || _.indexOf(this.collection.models, this.collection.get(currID));

        if( this.switching || this.collection.models.length < 2) {
            // We're currently switching previews or we don't have enough models, so ignore any pagination click events.
            return;
        }
        this.switching = true;

        if (data.direction === 'left' && (currID === _.first(this.collection.models).get('id')) ||
            data.direction === 'right' && (currID === _.last(this.collection.models).get('id'))) {
            this.switching = false;
            return;
        } else {
            // We can increment/decrement
            data.direction === 'left' ? currIndex -= 1 : currIndex += 1;

            //Reset the preview
            app.events.trigger('preview:render', this.collection.models[currIndex], this.collection, true);
            this.switching = false;
        }
    },

    /**
     * @deprecated Deprecated since 7.8.0. Will be removed in 7.10.0.
     */
    closePreview: function() {

        app.logger.warn('`Base.PreviewView#closePreview` has been deprecated since 7.8.0 and' +
            ' will be removed in 7.10.0.');

        if(_.isUndefined(app.drawer) || app.drawer.isActive(this.$el)){
            this.switching = false;
            delete this.model;
            this.collection.reset();
        }
    },

    bindDataChange: function() {
        if(this.collection) {
            this.collection.on('reset', this.filterCollection, this);
            // when remove active model from collection then close preview
            this.collection.on('remove', function(model) {
                if (model && this.model && (this.model.get('id') == model.get('id'))) {
                    // Remove the decoration of the highlighted row
                    app.events.trigger('list:preview:decorate', false);
                    // Close the preview panel
                    app.events.trigger('preview:close');
                }
            }, this);
        }
        // When the preview layout sets the new model in the context, the view
        // needs to switch the model and render for the fields to listen to the new
        // model changes.
        // Since the layout calls loadData, the fields will rerender when the data comes back
        // from the sever.
        this.context.on('change:model', function(ctx, model) {
            this.switchModel(model);
            this.render();
        }, this);
    },

    /**
     * When clicking on the pencil icon, toggle all editable fields
     * to edit mode
     */
    handleEdit: function() {
        this.setEditableFields();
        this.toggleFields(this.editableFields, true);
        this.toggleButtons(true);
        this.setButtonStates(this.STATE.EDIT);
        this.toggleLocks(true);
    },

    /**
     * Show or hide lock icons for locked fields
     *
     * @param {boolean} activate `true` to show lock icon on locked fields
     */
    toggleLocks: function(activate) {
        // Get the locked fields from the model
        var lockedFields = this.model.get('locked_fields') || [];

        if (!this._hasLockedFields) {
            return;
        }

        if (activate) {
            this.warnLockedFields();
        }
        _.each(this.fields, function(field) {
            if (_.contains(lockedFields, field.name)) {
                this.$('.preview-lock-link-wrapper[data-name=' + field.name + ']').toggleClass('hide', !activate);
            }
        }, this);
    },

    /**
     * Set a list of editable fields
     *
     * @override Overriding to checking field def if preview edit
     * is allowed
     */
    setEditableFields: function() {
        // Get the locked fields from the model
        var lockedFields = this.model.get('locked_fields') || [];

        // Clear any old locked fields that may have been set
        this._hasLockedFields = false;

        // we only want to edit non readonly fields
        this.editableFields = _.reject(this.fields, function(field) {
            // Locked fields should not be editable
            if (_.contains(lockedFields, field.name)) {
                this._hasLockedFields = true;
                return true;
            }
            return field.def.readOnly || field.def.calculated ||
                //Added for SugarLogic fields since they are not supported
                //Fixme: PAT-2241 will remove this
                field.def.previewEdit === false ||
                !app.acl.hasAccessToModel('edit', this.model, field.name);
        }, this);
    },

    /**
     * @inheritdoc
     */
    hasUnsavedChanges: function() {
        if (_.isUndefined(this.model)) {
            return false;
        }
        return this._super('hasUnsavedChanges');
    },

    /**
     * Returns the HTML body to use in the email.
     *
     * Ensure the result is a defined string and strip any signature wrapper
     * tags to ensure it doesn't get stripped if we insert a signature above
     * the forward content. Also strip any reply content class if this is a
     * forward to a previous reply. And strip any forward content class if this
     * is a forward to a previous forward.
     *
     * @see EmailClientLaunch plugin.
     * @param {Data.Bean} model Use this model when constructing the body.
     * @return {undefined|string}
     */
    emailOptionDescriptionHtml: function(model) {
        var tplHeaderHtml = this._getHeaderHtmlTemplate();
        var headerParams = this._getHeaderParams(model);
        var headerHtml = tplHeaderHtml(headerParams);
        var body = model.get('description_html') || '';

        body = body.replace('<div class="signature">', '<div>');
        body = body.replace('<div id="replycontent">', '<div>');
        body = body.replace('<div id="forwardcontent">', '<div>');

        return '<div></div><div id="' + this._contentId + '">' + headerHtml + body + '</div>';
    },
    /**
     * Returns the template for producing the header HTML for the top of the
     * forward content.
     *
     * @return {Function}
     * @private
     */
    _getHeaderHtmlTemplate: function() {
        // Use `this.def.type` because `this.type` was changed to `emailaction`
        // during initialization.
        this._tplHeaderHtml =
            app.template.getField('reply-action', this._tplHeaderHtmlName, this.module);

        return this._tplHeaderHtml;
    },

    /**
     * Get the data required by the header template.
     *
     * @param {Data.Bean} model The params come from this model's attributes.
     * EmailClientLaunch plugin should dictate the model based on the context.
     * @return {Object}
     * @protected
     */
    _getHeaderParams: function(model) {
        return {
            module: model.module,
            from: this._formatEmailList(model.get('from_collection')),
            date: model.get('date_sent'),
            to: this._formatEmailList(model.get('to_collection')),
            cc: this._formatEmailList(model.get('cc_collection')),
            name: model.get('name')
        };
    },
    /**
     * Given a list of people, format a text only list for use in a forward
     * header.
     *
     * @param {Data.BeanCollection} collection A list of models
     * @protected
     */
    _formatEmailList: function(collection) {
        return collection.map(function(model) {
            var name = model.get('parent_name') || '';

            if (_.isEmpty(name)) {
                return model.get('email_address') || '';
            }

            if (_.isEmpty(model.get('email_address'))) {
                return name;
            }

            return name + ' <' + model.get('email_address') + '>';
        }).join(', ');
    },
    emailOptionTo: function(model) {
        var originalTo;
        var originalSender = model.get('from_collection');
        var to = this._createRecipients(originalSender);
        return to;
    },
    /**
     * Returns the plain-text body to use in the email.
     *
     * @see EmailClientLaunch plugin.
     * @param {Data.Bean} model Use this model when constructing the body.
     * @return {undefined|string}
     */
    emailOptionDescription: function(model) {
        var headerParams;
        var header;
        var body;
        var description;

        // if (!this.useSugarEmailClient()) {
            headerParams = this._getHeaderParams(model);
            header = this._getHeader(headerParams);
            body = model.get('description') || '';
            description = '\n' + header + '\n' + body;
        // }

        return description;
    },
    emailOptionCc: function(model) {
        var originalCc;
        var cc;

        if (this.def.reply_all) {
            app.logger.warn('The reply_all option is deprecated. Use View.Fields.Base.Emails.ReplyAllActionField ' +
                'instead.');
            originalCc = model.get('cc_collection');
            cc = this._createRecipients(originalCc);
        }

        return cc;
    },

    /**
     * Create an array of email recipients from the collection, which can be
     * used as recipients to pass to the new email.
     *
     * @param {Data.BeanCollection} collection
     * @return {Array}
     * @private
     */
    _createRecipients: function(collection) {

        return collection.map(function(recipient) {
            var data = {
                email: app.data.createBean('EmailAddresses', {
                    id: recipient.get('email_address_id'),
                    email_address: recipient.get('email_address')
                })
            };

            // The type and id fields are not unset after a parent record
            // is deleted. So we test for name because the parent record is
            // truly only there if type and id are non-empty and the parent
            // record can be resolved and has not been deleted.
            if (recipient.get('parent') &&
                recipient.get('parent').type &&
                recipient.get('parent').id &&
                recipient.get('parent').name
            ) {
                // We omit type because it is actually the module name and
                // should be treated as an attribute.
                data.bean = app.data.createBean(
                    recipient.get('parent').type,
                    _.omit(recipient.get('parent'), 'type')
                );
            }

            return data;
        });
    },
    /**
     * Returns the attachments to use in the email.
     *
     * @see EmailClientLaunch plugin.
     * @param {Data.Bean} model Use this model when building the attachments.
     * @return {undefined|Array}
     */
    emailOptionAttachments: function(model) {
        return model.get('attachments_collection').map(function(attachment) {
            var filename = attachment.get('filename') || attachment.get('name');

            return {
                _link: 'attachments',
                upload_id: attachment.get('upload_id') || attachment.get('id'),
                name: filename,
                filename: filename,
                file_mime_type: attachment.get('file_mime_type'),
                file_size: attachment.get('file_size'),
                file_ext: attachment.get('file_ext')
            };
        });
    },
    /**
     * Build the header for text only emails.
     *
     * @param {Object} params
     * @param {string} params.from
     * @param {string} [params.date] Date original email was sent
     * @param {string} params.to
     * @param {string} [params.cc]
     * @param {string} params.name The subject of the original email.
     * @return {string}
     * @private
     */
    _getHeader: function(params) {
        var header = '-----\n' + app.lang.get('LBL_FROM', params.module) + ': ' + (params.from || '') + '\n';
        var date;

        if (params.date) {
            date = app.date(params.date).formatUser();
            header += app.lang.get('LBL_DATE', params.module) + ': ' + date + '\n';
        }

        header += app.lang.get('LBL_TO_ADDRS', params.module) + ': ' + (params.to || '') + '\n';

        if (params.cc) {
            header += app.lang.get('LBL_CC', params.module) + ': ' + params.cc + '\n';
        }

        header += app.lang.get('LBL_SUBJECT', params.module) + ': ' + (params.name || '') + '\n';

        return header;
    },

    /**
     * Returns the subject to use in the email.
     *
     * Any instances of "Re: ", "FW: ", and "FWD: " (case-insensitive) found at
     * the beginning of the subject are removed prior to applying the prefix.
     *
     * @see EmailClientLaunch plugin.
     * @param {Data.Bean} model Use this model when constructing the subject.
     * @return {undefined|string}
     */
    emailOptionSubject: function(model) {
        var pattern = /^((?:re|fw|fwd): *)*/i;
        var subject = model.get('name') || '';

        return app.lang.get(this._subjectPrefix, model.module) + ': ' + subject.replace(pattern, '');
    },
    /**
     * Makes a request to download the file based on the URL identified in the
     * attributes of the current target of the event.
     *
     * @param {Object} event DOM event.
     * @param {Object} event.currentTarget The current target of the event.
     * @private
     */
    _downloadFile: function(event) {
        var url = this.$(event.currentTarget).data('url');

        if (this.disposed === true) {
            return;
        }

        if (!_.isEmpty(url)) {
            app.api.fileDownload(url, {}, {iframe: this.getFieldElement()});
        }
    },
})

